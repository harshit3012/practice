import React from 'react';

const Konva = React.lazy(() => import('./views/konva/konva'));

const routes = [
  { path: '/konva', exact: true, name: 'Konva', component: Konva },

];

export default routes;