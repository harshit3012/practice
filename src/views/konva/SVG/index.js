import React, { Component, PropTypes, useState } from "react";
import Konva from "konva";
import useImage from "../../../util/use-image";
import { Image, Label, Transformer, Tag, Text, Group } from "react-konva";
import { resizeWithAspectRatio } from "../../../util/konvaHelpers";
import Form from "react-bootstrap/FormGroup";

const SVG = ({
  shapeProps,
  isSelected,
  onChange,
  onSelect,
  handleRemove,
  clipWidth,
  clipHeight,
  clipX,
  clipY,
}) => {
  const shapeRef = React.useRef();
  const trRef = React.useRef();

  React.useEffect(() => {
    if (isSelected) {
      // we need to attach transformer manually
      trRef.current.setNode(shapeRef.current);
      trRef.current.getLayer().batchDraw();
    }
  }, [isSelected]);
  const [image] = useImage(shapeProps.url, "Anonymous");
  const [isDragging, setIsDragging] = useState(false);

  if (image && !shapeProps.width && !shapeProps.height) {
    if (image.width > 100 || image.height > 100) {
      let width = image.width;
      let height = image.height;
      let [newWidth, newHeight] = resizeWithAspectRatio(
        width,
        height,
        100,
        100
      );
      image.width = newWidth;
      image.height = newHeight;
      shapeRef.current.setAttrs({
        width: newWidth,
        height: newHeight,
      });
    }
  }

  return (
    <React.Fragment>
      {!isDragging && (
        <Label
          visible={isSelected}
          scaleX={1.5}
          scaleY={1.5}
          offsetX={10}
          offsetY={10}
          onClick={handleRemove}
          {...shapeProps}
          draggable={false}
        >
          <Tag fill="" />
          <Text text="X" fill={shapeProps.fill} />
        </Label>
      )}

      <Group
        clipWidth={clipWidth}
        clipHeight={clipHeight}
        clipX={clipX}
        clipY={clipY}
      >
        <Image
          ref={shapeRef}
          x={shapeProps.x}
          y={shapeProps.y}
          image={image}
          filters={[Konva.Filters.RGB]}
          height={shapeProps.height}
          width={shapeProps.width}
          draggable={true}
          onClick={onSelect}
          onDragEnd={(e) => {
            onChange({
              ...shapeProps,
              x: e.target.x(),
              y: e.target.y(),
            });
            setIsDragging(false);
          }}
          onDragStart={() => setIsDragging(true)}
          onTransformEnd={(e) => {
            const node = shapeRef.current;
            const scaleX = node.scaleX();
            const scaleY = node.scaleY();

            node.scaleX(1);
            node.scaleY(1);
            onChange({
              ...shapeProps,
              x: node.x(),
              y: node.y(),
              width: Math.max(5, node.width() * scaleX),
              height: Math.max(node.height() * scaleY),
            });
          }}
        />
      </Group>

      {isSelected && (
        <Transformer
          ref={trRef}
          boundBoxFunc={(oldBox, newBox) => {
            // limit resize
            if (newBox.width < 5 || newBox.height < 5) {
              return oldBox;
            }
            return newBox;
          }}
        />
      )}
    </React.Fragment>
  );
};

export default SVG;
