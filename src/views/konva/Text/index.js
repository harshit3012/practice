import React from 'react';
import { Text } from 'react-konva';

class KonvaText extends React.Component {
  state = {
    textInput : {
      text: '',
      fontFamily: '',
      color: '',
      fontSize: ''
    }
  };

  componentDidUpdate(oldProps) {
    if (oldProps.textInput != this.props.textInput) {
      this.setState({
        textInput: this.props.textInput
      })
    }
  }

  render() {
    return (
      <Text
        text={this.state.textInput.text}
        fontSize={this.state.textInput.fontSize}
        fill={this.state.textInput.color}
        fontFamily='Helvetica'
        draggable={true}
      />
    );
  }
}

export default KonvaText;