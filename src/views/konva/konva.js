import React, { useState } from 'react';
import { Layer, Stage, Rect, Image, Label } from "react-konva";
import { Col, Input, Row } from 'reactstrap';
import useImage from 'use-image';
import URLImage from './Image/index';
import KonvaText from './Text/index';

const LionImage = () => {
    const [image] = useImage('https://konvajs.org/assets/lion.png');
    return (
        <Image
            image={image}
            draggable={true} />
    );
};


function Konva() {

    const stageRef = React.useRef();
    const layerRef = React.useRef();
    const rectRef = React.useRef();
    const containerRef = React.useRef();

    const [pageProps, setPageProps] = useState({
        stageWidth: 400,
        stageHeight: 500,
        clipWidth: 200,
        clipHeight: 300,
        clipX: 100,
        clipY: 100,
        canvas: {
            width: 0,
            height: 0,
            color: "red",
        },
        boundryVisible: true
    });

    const [imageRotate, setImageRotate] = useState(0);
    const [textInput, setTextInput] = useState({
        text: '',
        fontFamily: '',
        color: '',
        fontSize: ''
    });

    const handleRotationChange = (e) => {
        setImageRotate(e.target.value);
    }

    const handleTextChange = (e) => {
        setTextInput({
            ...textInput,
            [e.target.name]: e.target.value
        })
    }
console.log(textInput, "---textInput---58")
    return (
        <>
            <div>
                <Row style={{ display: 'flex' }} >
                    <Col md='6' lg='6' style={{ width: '600px', border: '1px dotted black' }}>
                        <h1>Konva JS</h1>
                        <Stage
                            width={pageProps.stageWidth}
                            height={pageProps.stageHeight}
                            ref={stageRef} >
                            <Layer ref={layerRef} >
                                {/* <Rect
                                    ref={rectRef}
                                    visible={pageProps.boundryVisible}
                                    dash={[2, 2]}
                                    width={pageProps.clipWidth}
                                    height={pageProps.clipHeight}
                                    x={pageProps.clipX}
                                    y={pageProps.clipY}
                                    stroke="black"
                                    strokeWidth={10}
                                    fill='green'
                                    dashEnabled
                                    draggable={true}
                                    dragBoundFunc={function (pos) {
                                        var newY = pos.y < 300 ? 300 : pos.y;
                                        var newX = pos.x < 300 ? 300 : pos.x;
                                        return {
                                            x: newX,
                                            y: newY
                                        }
                                    }}
                                /> */}

                                <URLImage
                                    src="https://konvajs.org/assets/yoda.jpg"
                                    x={150} newAngle={imageRotate} />
                                <LionImage />

                                <KonvaText textInput={textInput} />

                            </Layer>
                        </Stage>
                    </Col>

                    <Col md='6' lg='6' style={{ width: '600px', border: '1px dotted black' }}>
                        <Row>
                            <Col style={{ margin: '25px' }}>
                                <Label>Rotate:  </Label>
                                <Input type='number' value={imageRotate} onChange={(e) => handleRotationChange(e)} />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ margin: '25px' }}>
                                <Label>Enter Text:  </Label>
                                <Input type='text' name='text' value={textInput.text} onChange={(e) => handleTextChange(e)} />
                            </Col>
                            <Col style={{ margin: '25px' }}>
                                <Label>Font Style:  </Label>
                                <Input type='select' name='fontFamily' value={textInput.fontFamily} onChange={(e) => handleTextChange(e)} >
                                    <option value='Arial, sans-serif' >Arial</option>
                                    <option value='Helvetica, sans-serif' >Helvetica</option>
                                    <option value='Verdana, sans-serif' >Verdana</option>
                                    <option value='Trebuchet MS, sans-serif' >Trebuchet MS</option>
                                    <option value='Gill Sans, sans-serif' >Gill Sans</option>
                                    <option value='Noto Sans, sans-serif' >Noto Sans</option>
                                    <option value='Arial Narrow, sans-serif' >Arial Narrow</option>
                                </Input>
                            </Col>
                            <Col style={{ margin: '25px' }}>
                                <Label>Font Color:  </Label>
                                <Input type='select' name='color' value={textInput.color} onChange={(e) => handleTextChange(e)} >
                                    <option value='green' >Green</option>
                                    <option value='black' >Black</option>
                                    <option value='red' >Red</option>
                                    <option value='blue' >Blue</option>
                                </Input>
                            </Col>
                            <Col style={{ margin: '25px' }}>
                                <Label>Enter Text Size:  </Label>
                                <Input type='number' name='fontSize' value={textInput.fontSize} onChange={(e) => handleTextChange(e)} />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>

        </>
    )

}

export default Konva;