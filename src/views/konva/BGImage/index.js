import React, { Component } from 'react';
import { Image } from 'react-konva';

class BGImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null
    };

    this.imageNode = React.createRef();
  }
  componentDidMount() {
    this.loadImage();
  }

  componentDidUpdate(prevProps) {
    
    if(prevProps.src != this.props.src){
      this.loadImage();
      this.updateImagePosition();
    }
    
  }

  updateImagePosition() {
    // center image and resize to fit the stage container
    if (this.imageNode.current && this.props.stageRef.current && this.imageNode.current.getWidth()) {
      const imgWidth = this.imageNode.current.getWidth()
      const stageWidth = this.props.stageRef.current.getWidth();
      const imgHeight = this.imageNode.current.getHeight();
      const stageHeight = this.props.stageRef.current.getHeight();

      let imageAspectRatio = imgWidth / imgHeight;
      let stageAspectRatio = stageWidth / stageHeight;
      let renderableHeight, renderableWidth, xStart, yStart;

      // If image's aspect ratio is less than canvas's we fit on height
      // and place the image centrally along width
      if (imageAspectRatio < stageAspectRatio) {
        renderableHeight = stageHeight;
        renderableWidth = imgWidth * (renderableHeight / imgHeight);
        xStart = (stageWidth - renderableWidth) / 2;
        yStart = 0;
      }

      // If image's aspect ratio is greater than canvas's we fit on width
      // and place the image centrally along height
      else if (imageAspectRatio > stageAspectRatio) {
        renderableWidth = stageWidth
        renderableHeight = imgHeight * (renderableWidth / imgWidth);
        xStart = 0;
        yStart = (stageHeight - renderableHeight) / 2;
      }

      // Happy path - keep aspect ratio
      else {
        renderableHeight = stageHeight;
        renderableWidth = stageWidth;
        xStart = 0;
        yStart = 0;
      }
      this.imageNode.current.setAttrs({
        x: xStart,
        y: yStart,
        width: renderableWidth,
        height: renderableHeight,
      })
    }
  }

  loadImage() {
    this.image = new window.Image();
    this.image.setAttribute('crossOrigin', 'anonymous');
    this.image.src = this.props.src;
    this.image.addEventListener('load', this.handleLoad);
  }
  handleLoad = () => {
    this.setState({
      image: this.image
    });
    this.updateImagePosition();
  };

  render() {

    return (
      <Image
        ref={this.imageNode}
        draggable={false}
        image={this.state.image}
        onClick={this.props.handleClick}
      />
    )
  }
}

export default BGImage;