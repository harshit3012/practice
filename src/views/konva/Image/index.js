import React from 'react';
import { Image } from 'react-konva';

class URLImage extends React.Component {
  state = {
    image: null,
    imageRotate: 0
  };

  componentDidMount() {
    this.loadImage();
  }

  componentDidUpdate(oldProps) {
    if (oldProps.src !== this.props.src) {
      this.loadImage();
    }
    if (oldProps.newAngle != this.props.newAngle) {
      this.setState({
        imageRotate: this.props.newAngle
      })
    }
  }

  componentWillUnmount() {
    this.image.removeEventListener('load', this.handleLoad);
  }

  loadImage() {
    this.image = new window.Image();
    this.image.src = this.props.src;
    this.image.addEventListener('load', this.handleLoad);
  }

  handleLoad = () => {
    this.setState({
      image: this.image
    });
  };

  render() {
    return (
      <Image
        x={this.props.x}
        y={this.props.y}
        image={this.state.image}
        ref={node => {
          this.imageNode = node;
        }}
        draggable={true}
        rotation={this.state.imageRotate}
      />
    );
  }
}

export default URLImage;