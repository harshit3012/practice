import {
  CLOUDINARY_UPLOAD_API,
  CLOUD,
  API_ROOT,
  URI,
  postApiHeader,
  APP_ROOT,
  tokenExpired,
  setRefreshToken,
  TOKEN_KEY,
  ErrorOnApi,
} from "../config";
import { updateIsLoading, isImageLoading } from "../actions/app";
import JSZipUtils from "jszip-utils";

export const importAll = (r) => {
  return r.keys().map(r);
};

export const dataURItoBlob = (dataURI) => {
  var mime = dataURI
    .split(",")[0]
    .split(":")[1]
    .split(";")[0];
  var binary = atob(dataURI.split(",")[1]);
  var array = [];
  for (var i = 0; i < binary.length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: mime });
};

export const getUserDetails = () => {
  return JSON.parse(localStorage.getItem("userDetails"));
};

export const uploadImageToCloudinary = async (file) => {
  let fd = new FormData();
  fd.append("upload_preset", CLOUD.CLOUD_PRESET);
  fd.append("file", file);

  return fetch(CLOUDINARY_UPLOAD_API, {
    method: "POST",
    headers: {
      "X-Requested-With": "XMLHttpRequest",
    },
    body: fd,
  })
    .then((res) => res.json())
    .then((data) => {
      if (data && data.secure_url) {
        return data.secure_url;
      } else {
        return null;
      }
    })
    .catch((err) => {
      return null;
    });
};

export const uploadImageToServer = async (file) => {
  console.log("-file-----------", file);
  // const APP_URL="https://waapi.waaflow.com"
  const APP_URL = APP_ROOT;
  let payload = {};
  payload.file = file;
  const saveImageRequest = postApiHeader(payload);
  isImageLoading(true);
  return fetch(API_ROOT + URI.UPLOAD_IMAGE, saveImageRequest)
    .then((res) => {
      if (res.status == 200) {
        const token = res.headers.get(TOKEN_KEY);
        setRefreshToken(token);
        return res.json();
      } else if (res.status == 401) {
        tokenExpired();
      } else {
        ErrorOnApi();
      }
    })
    .then((data) => {
      isImageLoading(false);
      if (data) {
        return `${APP_URL}/${data}`;
      }
    })
    .catch((err) => {
      isImageLoading(false);
      return null;
    });
};

export const urlToPromise = (url) => {
  return new Promise(function(resolve, reject) {
    JSZipUtils.getBinaryContent(url, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};


export const getBase64Image = (imgUrl) => {
  return new Promise((resolve, reject) => {
    let img = new Image();

    // onload fires when the image is fully loadded, and has width and height

    img.onload = () => {
      let canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;
      let ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);
      let dataURL = canvas.toDataURL("image/png");
      // dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
      resolve(dataURL); // the base64 string
    };

    // set attributes and src
    img.setAttribute("crossOrigin", "anonymous"); //
    img.src = imgUrl;
  });
};

export const toBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
};


export const checkIfImageExist = (url) => {
  return new Promise((res, rej ) => {
    return fetch(url).then(response => {
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.startsWith("image")) {
       res()
      } else {
        rej()
      }
    });
  })
}