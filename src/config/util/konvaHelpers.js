module.exports.resizeWithAspectRatio = (width, height, maxWidth, maxHeight) => {
  let ratio = 0; // Used for aspect ratio
  let newWidth = 0;
  let newHeight = 0;

  // Check if the current width is larger than the max
  if (width > maxWidth) {
    ratio = maxWidth / width; // get ratio for scaling image
    newWidth = maxWidth; // Set new width
    newHeight = height * ratio; // Scale height based on ratio
    height = height * ratio; // Reset height to match scaled image
    width = width * ratio; // Reset width to match scaled image
  }

  // Check if current height is larger than max
  if (height > maxHeight) {
    ratio = maxHeight / height; // get ratio for scaling image
    newHeight = maxHeight; // Set new height
    newWidth = width * ratio; // Scale width based on ratio
    width = width * ratio; // Reset width to match scaled image
    height = height * ratio; // Reset height to match scaled image
  }

  return [newWidth, newHeight];
};
