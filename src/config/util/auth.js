// role -- 1 for admin
// role -- 2 for user

export const CONSTANTS = {
  CUSTOMIZE_BUTTON: 'CUSTOMIZE_BUTTON',
  ADD_PRODUCT: 'ADD_PRODUCT'
}

const AllowedOperations = {
  'CUSTOMIZE_BUTTON': [2],
  'ADD_PRODUCT': [1]
}

export const shouldVisible = (operation) => {
  const userDetails = JSON.parse(localStorage.getItem('userDetails'));
  if (!userDetails) {
    return false;
  }

  const roleId = userDetails.role;
  let allowedRoles = AllowedOperations[operation];
  return allowedRoles.indexOf(roleId) > -1
}

