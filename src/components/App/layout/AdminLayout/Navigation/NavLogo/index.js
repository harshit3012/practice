import React from 'react';
import { DEMO } from './../../../../../constants';
import Aux from "../../../../../hoc/_Aux";
import { APP } from '../../../../../config';
import menuIcon from '../../../../../assets/logo-waaflow-white.png';
const navLogo = (props) => {
  let toggleClass = ['mobile-menu'];
  if (props.collapseMenu) {
    toggleClass = [...toggleClass, 'on'];
  }

  return (
    <Aux>
      <div className="navbar-brand header-logo">
        <a href={DEMO.BLANK_LINK} className="b-brand">
          <div >
            {/* <i className="feather icon-trending-up" /> */}
            <img src={menuIcon} style={{width: '166px'}}/>
          </div>
          {/* <span className="b-title">{APP.NAME}</span> */}
        </a>
        <a href={DEMO.BLANK_LINK} className={toggleClass.join(' ')} id="mobile-collapse" onClick={props.onToggleNavigation}>
          <span />
          </a>
      </div>
    </Aux>
  );
};

export default navLogo;
