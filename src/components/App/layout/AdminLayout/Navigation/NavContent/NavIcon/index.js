import React from 'react';
import ProductSvgIcon from '../../../../../../assets/images/productIcon.svg';
const navIcon = (props) => {
    let navIcons = false;
    if (props.items.icon) {
        navIcons = <span className="pcoded-micon"><i className={props.items.icon} /></span>;
    }
    if(props.items.img){
        navIcons = <span className="pcodedmicon"><img style={{height: '2rem',paddingLeft:5}}src={ProductSvgIcon}/></span>;
    }

    return navIcons;
};

export default navIcon;