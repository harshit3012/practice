import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown } from 'react-bootstrap';
import windowSize from 'react-window-size';

import NavSearch from './NavSearch';
import Aux from "../../../../../hoc/_Aux";
import { DEMO } from "../../../../../constants";
import { THEME } from "../../../../../constants";
import EnglishLanguage from '../../../../../config/en-gb.json';
import FrenchLanguage from '../../../../../config/fr-fr.json';


class NavLeft extends Component {

  changeLanguage = (type) => {
    if (type == 'EN') {
      localStorage.setItem("language", JSON.stringify(EnglishLanguage));
      localStorage.setItem("languageType", 'EN');
    } else {
      localStorage.setItem("language", JSON.stringify(FrenchLanguage));
      localStorage.setItem("languageType", 'FR');
    }
    window.location.reload(false);
  }

  render() {
    let iconFullScreen = ['feather'];
    iconFullScreen = (this.props.isFullScreen) ? [...iconFullScreen, 'icon-minimize'] : [...iconFullScreen, 'icon-maximize'];

    let navItemClass = ['nav-item'];
    if (this.props.windowWidth <= 575) {
      navItemClass = [...navItemClass, 'd-none-'];
    }
    let dropdownRightAlign = false;
    if (this.props.rtlLayout) {
      dropdownRightAlign = true;
    }

    return (
      <Aux>
        <ul className="navbar-nav mr-auto">
          <li><a href={DEMO.BLANK_LINK} className="full-screen" onClick={this.props.onFullScreen}><i className={iconFullScreen.join(' ')} /></a></li>
          <li className={navItemClass.join(' ')}>
            <Dropdown alignRight={dropdownRightAlign}>
              <Dropdown.Toggle variant={'link'} id="dropdown-basic">
                {localStorage.getItem('languageType')}
              </Dropdown.Toggle>
              <ul>
                <Dropdown.Menu>
                  <li style={{display:'block'}} onClick={() => this.changeLanguage('EN')}>
                    <a className="dropdown-item cursor-pointer">English</a>
                  </li>
                  <li style={{display:'block'}} onClick={() => this.changeLanguage('FR')}>
                    <a className="dropdown-item cursor-pointer">French</a>
                  </li>
                </Dropdown.Menu>
              </ul>
            </Dropdown>
          </li>
          {/* <li className="nav-item"><NavSearch /></li> */}
        </ul>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    isFullScreen: state.theme.isFullScreen,
    rtlLayout: state.theme.rtlLayout
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFullScreen: () => dispatch({ type: THEME.FULL_SCREEN }),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(windowSize(NavLeft));
