import React from 'react';
import { Button } from 'reactstrap';

function AdminLayout(props) {

  return (
    <>
      <div style={{ margin: '25px' }}>
        <Button color='primary' onClick={() => props.history.push('/konva')} > Konva </Button>
      </div>
    </>
  )

}

export default AdminLayout;